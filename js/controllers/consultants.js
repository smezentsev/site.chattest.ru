(function(){
    'use strict';


    angular.module('app', ['wu.masonry']).
        controller('ConsultantsCtrl', function ($scope) {
            function genBrick() {
                var height = ~~(Math.random() * 500) + 100;
                var id = ~~(Math.random() * 10000);
                return [{
                    src: 'http://lorempixel.com/g/280/' + height + '/?' + id,
                    height: height
                }];
            };

            $scope.add = function add() {
                $scope.questions.push(
                    {
                        message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials.',
                        pictures: genBrick()
                    }

                );
            };

            $scope.remove = function remove() {
                $scope.bricks.splice(
                    ~~(Math.random() * $scope.bricks.length),
                    1
                )
            };


            $scope.questions = [
                {
                    id: 1,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 2,
                    message: '  This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 3,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 4,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 1,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 2,
                    message: ' I am trying to get angular-masonry by passy to work with an infinite scroll directive, but I am having some problems. I am doing it in a plnkr here. It says in the console as an error, TypeError: Object [object Object]',
                    pictures: genBrick()

                },
                {
                    id: 3,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 4,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 1,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 2,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 3,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },
                {
                    id: 4,
                    message: ' As of Angular 1.2.0 the ngInclude scripts does not permit execution of javascript from included partials. This little module execute code inside script tags with "javascript-lazy" attribute after partial loading, thus re-enabling this feature.',
                    pictures: genBrick()

                },

            ]


        });

})();